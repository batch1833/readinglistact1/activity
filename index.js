//3.Create a function that will accept the name of the user and will print each character of the name in reverse order. If the length of the name is more than seven characters, the consonants of the name will only be printed in the console.



//4.Create an array of students with the following value.
//1.) Total size of an array.
let students = ['John', 'Jane', 'Joe'];
console.log(students);

//2.) Adding a new element at the beginning of an array. (Add Jack)
students.unshift('jack');
console.log(students);
//3.) Adding a new element at the end of an array. (Add Jill)
students.push('jill');
console.log(students);
//4.) Removing an existing element at the beginning of an array 
students.shift('jack');
console.log(students);
//5.) Removing an existing element at the end of an array.
students.pop('jill');
console.log(students);
//6.) Find the index number of the last element in an array.
lastIndex = students.length - 1
console.log(students[lastIndex]);
//7.) Print each element of the array in the browser console.
console.log(students);




//5.Using the students array, create a function that will look for the index of a given student and will remove the name of the student from the array list.
let names = ['John', 'Jane', 'Joe'];

// get input name
let usrName = prompt("Enter your Name: ");

const userIndexToDel = names.indexOf(usrName);

names.splice(userIndexToDel,1);
if (userIndexToDel > -1) {
  names.splice(userIndexToDel,1);
  console.log(names);
}
else {
  console.log('User does not exist.')
}


//6.Create a "person" object that contains properties first name, last name, nickname, age, address(contains city and country), friends(with at least 3 friends with the first name), and a function property that contains console.log() displaying text about self introduction about name, age, address and who is his/her friends.

let person = {
	firstName: "Johnny ",
	lastName: "Deep",
	nickName: "John",
	age: 40 ,
	address: {
		city: 'Manila',
		country: 'Philippines'
	},
	friends:['Amber', 'James', 'Josh'],
	printDetails: function(){
			console.log("My name is " + person.firstName +"but you can call me " +" I am " + person.age + " I live in "+ person.address.city+ ", " + person.address.country + " I am with my " + person.friends);

		}
	}

person.printDetails();


	
//7.

